# Python program to create a simple GUI  
# calculator using Tkinter
# by: Thomas Rode
# date: 3/7/2019 
  
# import everything from tkinter module 
from tkinter import *

# globally declare the expression variable 
expression = ""

# globally declare these variables 
total = 0
mem1 = 0
mem2 = 0
#RawValForMemory = 0
#RawValFlg = 0

# globally change button height and width and font
HEIGHT = 2
WIDTH = 9
BTNFONT = "Arial 17 bold"

# globally set the configuration of GUI window (Width x Length)
GUI_HEIGHT_WIDTH = ("680x630")#("530x560")#("530x490")


# Math class
class AdvMathFunc:

    def __init__(self,x,y):
        self.x = x
        self.y = y

    def get_x(self):
        return self.x

    def set_x(self, x):
        self.x = x

    def get_y(self):
        return self.y

    def set_y(self, y):
        self.y = y

    def Sub(self):
        return (float(self.x) - float(self.y))

    def Add(self):
        return (float(self.x) + float(self.y))

    def Mul(self):
        return (float(self.x) * float(self.y))

    def Div(self):
        return (float(self.x) / float(self.y))

    # Raise to power of 'x^y'
    def RaiseToPower(self):
        z = self.x
        for i in range(1,self.y):
            z = self.x * z
        return z

    # Square root x^2
    def RootSquared(self):
        return (self.x * self.x)

    # Decimal to Hexidecimal conversion
    def Dec2hex(self):
        return hex(self.x)

    # Hexidecimal to decimal conversion
    def Hex3dec(self):
        return int(self.x,16)

    # Left shift conversion returns in decimal 
    def Lsh(self):
        return int(self.x, 16) << int(self.y, 16)

    # Right shift conversion returns in decimal 
    def Rsh(self):
        return int(self.x, 16) >> int(self.y, 16)

    # Or operator returns in decimal 
    def Or(self):
        return int(self.x, 16) | int(self.y, 16)

    # Xor operator returns in decimal 
    def Xor(self):
        return int(self.x, 16) ^ int(self.y, 16)

    # Not or complement operator returns in decimal 
    def Not(self):
        x = int(self.x, 16)
        x = ~x
        return x
    
    # And x and y 
    def And(self):
        return int(self.x, 16) & int(self.y, 16)

def memoryset(mem):

    global total
    global mem1
    global mem2
    #global RawValForMemory
    #global RawValFlg 

    if 'mem1' in mem:

        # check to see if number just hit
        #if RawValFlg == 1:
        #    mem1 = RawValForMemory
        #    RawValFlg = 0
        #else: 
        mem1 = total
        equation.set("Memory1 set")
        

    elif 'mem2' in mem:
        equation.set("Memory2 set")
        mem2 = total

def memoryrecall(mem):
    global total
    global mem1
    global mem2

    if 'mem1' in mem:   
        total = mem1
        # clear out mem1 buffer
        mem1 = 0

    elif 'mem2' in mem:
        total = mem2
        # clear out mem2 buffer
        mem2 = 0

    # call press and add this into the string
    press(total)

    #Clearout total
    total = 0
    
  
# Function to update expressiom 
# in the text entry box 
def press(num): 
    # point out the global expression variable 
    global expression
    #global RawValForMemory
    #global RawValFlg 
    
    # Check if just numeric values pressed
    #if '1' in num or '2' in num or '3' in num or '4' in num or '5' in num or '6' in num or '7' in num or '8' in num or '9' in num or '0' in num:           
        #RawValForMemory = str(RawValForMemory) + str(num)
        #RawValFlg = 1
    
    # original below
    # concatenation of string
    expression = expression + str(num)
    
    # update the expression by using set method 
    equation.set(expression)
   
  
# Function to evaluate the final expression 
def equalpress(): 
    # Try and except statement is used 
    # for handling the errors like zero 
    # division error etc. 
  
    # Put that code inside the try block 
    # which may generate the error

    #Initialize an instance for the advance math class
    CalcOp = AdvMathFunc(int(0), int(0))
    
    try: 
  
        global expression
        # point out the total expression variable 
        global total 
  
        # eval function evaluate the expression 
        # and str function convert the result 
        # into string

        # Square root
        if '^2' in expression:
            
            # parse the incoming sting before '^2'
            x = expression.replace("^2", "")

            # set x into class
            CalcOp.set_x(int(x))
             
            # calculate the square root
            total = CalcOp.RootSquared() 
          
        # Raise to power of
        elif 'x^y' in expression:

            # parse the incoming string before 'x^y'
            x = expression.split("x^y")[0]
            
            # parse incoming string after 'x^y'           
            y = expression.split("x^y")[1]

            # set x and y into class
            CalcOp.set_x(int(x))
            CalcOp.set_y(int(y))

            # calculate raise to a power
            total = CalcOp.RaiseToPower()

        # convert Decimal to Hexidecimal
        elif 'dec2hex' in expression:

            # parse the incoming sting before 'dec2hex'
            x = expression.replace("dec2hex", "")

            # set x into class
            CalcOp.set_x(int(x))

            # calculate conversion
            total = CalcOp.Dec2hex()

        # convert Hexidecimal to decimal
        elif 'hex2dec' in expression:

            # parse the incoming sting before 'dec2hex'
            x = expression.replace("hex2dec", "")
           
            # set x into class
            CalcOp.set_x(x)
            
            # calculate conversion
            total = CalcOp.Hex3dec()

        # Left shift 'x' by 'y' amount operator
        elif 'Lsh' in expression:
            # parse the incoming string before 'x^y'
            x = expression.split("Lsh")[0]
            
            # parse incoming string after 'x^y'           
            y = expression.split("Lsh")[1]

            # set x and y into class
            CalcOp.set_x(x)
            CalcOp.set_y(y)

            # left shift value
            total = CalcOp.Lsh()

        # Right shift 'x' by 'y' amount operator
        elif 'Rsh' in expression:
            # parse the incoming string before 'x^y'
            x = expression.split("Rsh")[0]
            
            # parse incoming string after 'x^y'              
            y = expression.split("Rsh")[1]

            # set x and y into class
            CalcOp.set_x(x)
            CalcOp.set_y(y)

            # right shift value
            total = CalcOp.Rsh()

        #  'x' Or with 'y' amount operator
        elif 'Or' in expression:
            # parse the incoming string before 'x^y'
            x = expression.split("Or")[0]
            
            # parse incoming string after 'x^y'           
            y = expression.split("Or")[1]

            # set x and y into class
            CalcOp.set_x(x)
            CalcOp.set_y(y)

            # right shift value
            total = CalcOp.Or()

        #  'x' Or with 'y' amount operator
        elif 'Xor' in expression:
            # parse the incoming string before 'x^y'
            x = expression.split("Xor")[0]
            
            # parse incoming string after 'x^y'           
            y = expression.split("Xor")[1]

            # set x and y into class
            CalcOp.set_x(x)
            CalcOp.set_y(y)

            # right shift value
            total = CalcOp.Xor()

        # Compliment or Not 'x'
        elif 'Not' in expression:

            # parse the incoming sting before 'Not'
            x = expression.replace("Not", "")
            
            # set x into class
            CalcOp.set_x(x)
            
            # calculate conversion
            total = CalcOp.Not()

        #  'x' Anded with 'y' amount operator
        elif 'And' in expression:
            # parse the incoming string before 'x^y'
            x = expression.split("And")[0]
            
            # parse incoming string after 'x^y'           
            y = expression.split("And")[1]

            # set x and y into class
            CalcOp.set_x(x)
            CalcOp.set_y(y)

            # right shift value
            total = CalcOp.And()

        #  'x' Added with 'y' amount operator
        elif '+' in expression:
            # parse the incoming string before 'x^y'
            x = expression.split("+")[0]
            
            # parse incoming string after 'x^y'           
            y = expression.split("+")[1]

            # set x and y into class
            CalcOp.set_x(x)
            CalcOp.set_y(y)

            # right shift value
            total = CalcOp.Add()

        #  'x' subtracted with 'y' amount operator
        elif '-' in expression:
            # parse the incoming string before 'x^y'
            x = expression.split("-")[0]
            
            # parse incoming string after 'x^y'           
            y = expression.split("-")[1]

            # set x and y into class
            CalcOp.set_x(x)
            CalcOp.set_y(y)

            # right shift value
            total = CalcOp.Sub()

        #  'x' multiplied with 'y' amount operator
        elif '*' in expression:
            # parse the incoming string before 'x^y'
            x = expression.split("*")[0]
            
            # parse incoming string after 'x^y'           
            y = expression.split("*")[1]

            # set x and y into class
            CalcOp.set_x(x)
            CalcOp.set_y(y)

            # right shift value
            total = CalcOp.Mul()

        #  'x' divided with 'y' amount operator
        elif '/' in expression:
            # parse the incoming string before 'x^y'
            x = expression.split("/")[0]
            
            # parse incoming string after 'x^y'           
            y = expression.split("/")[1]

            # set x and y into class
            CalcOp.set_x(x)
            CalcOp.set_y(y)

            # right shift value
            total = CalcOp.Div()
            
        # No specific operation therefore store value 
        else:
            # Deprecated 
            #total = str(eval(expression))
            total = str(expression)
  
        equation.set(total) 
  
        # initialze the expression variable 
        # by empty string 
        expression = ""
        
    # if error is generate then handle 
    # by the except block 
    except: 
  
        equation.set(" error ") 
        expression = "" 
  
  
# Function to clear the contents 
# of text entry box 
def clear(): 
    global expression 
    expression = "" 
    equation.set("")
  
# Driver code 
if __name__ == "__main__": 
    # create a GUI window 
    gui = Tk() 
  
    # set the background colour of GUI window 
    gui.configure(background="gold") 
  
    # set the title of GUI window 
    gui.title(" Big Button Calculator ") 
  
    # set the configuration of GUI window (Width x Length)
    gui.geometry(GUI_HEIGHT_WIDTH)
    
    # StringVar() is the variable class 
    # we create an instance of this class 
    equation = StringVar() 
  
    # create the text entry box for 
    # showing the expression .
    large_font = ('Verdana',20)
    expression_field = Entry(gui, textvariable=equation, font=large_font)
  
   
    # grid method is used for placing 
    # the widgets at respective positions 
    # in table like structure . 
    expression_field.grid(columnspan=5, ipadx=70) 
    
    
    equation.set('Enter Your Expression') 
  
    # create a Buttons and place at a particular 
    # location inside the root window . 
    # when user press the button, the command or 
    # function affiliated to that button is executed . 
    button1 = Button(gui, text=' 1 ', fg='black', bg='red', font=BTNFONT, 
                     command=lambda: press(1), height=HEIGHT, width=WIDTH)
    button1.grid(row=3, column=0) 
  
    button2 = Button(gui, text=' 2 ', fg='black', bg='red', font=BTNFONT,
                     command=lambda: press(2), height=HEIGHT, width=WIDTH) 
    button2.grid(row=3, column=1) 
  
    button3 = Button(gui, text=' 3 ', fg='black', bg='red', font=BTNFONT, 
                     command=lambda: press(3), height=HEIGHT, width=WIDTH) 
    button3.grid(row=3, column=2) 
  
    button4 = Button(gui, text=' 4 ', fg='black', bg='red', font=BTNFONT,
                     command=lambda: press(4), height=HEIGHT, width=WIDTH) 
    button4.grid(row=4, column=0) 
  
    button5 = Button(gui, text=' 5 ', fg='black', bg='red', font=BTNFONT,
                     command=lambda: press(5), height=HEIGHT, width=WIDTH) 
    button5.grid(row=4, column=1) 
  
    button6 = Button(gui, text=' 6 ', fg='black', bg='red', font=BTNFONT,
                     command=lambda: press(6), height=HEIGHT, width=WIDTH) 
    button6.grid(row=4, column=2) 
  
    button7 = Button(gui, text=' 7 ', fg='black', bg='red', font=BTNFONT,
                     command=lambda: press(7), height=HEIGHT, width=WIDTH) 
    button7.grid(row=5, column=0) 
  
    button8 = Button(gui, text=' 8 ', fg='black', bg='red', font=BTNFONT,
                     command=lambda: press(8), height=HEIGHT, width=WIDTH) 
    button8.grid(row=5, column=1) 
  
    button9 = Button(gui, text=' 9 ', fg='black', bg='red', font=BTNFONT,
                     command=lambda: press(9), height=HEIGHT, width=WIDTH) 
    button9.grid(row=5, column=2) 
  
    button0 = Button(gui, text=' 0 ', fg='black', bg='red', font=BTNFONT,
                     command=lambda: press(0), height=HEIGHT, width=WIDTH) 
    button0.grid(row=6, column=0) 
  
    plus = Button(gui, text=' + ', fg='black', bg='red', font=BTNFONT,
                  command=lambda: press("+"), height=HEIGHT, width=WIDTH) 
    plus.grid(row=3, column=3) 
  
    minus = Button(gui, text=' - ', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press("-"), height=HEIGHT, width=WIDTH) 
    minus.grid(row=4, column=3) 
  
    multiply = Button(gui, text=' * ', fg='black', bg='red', font=BTNFONT,
                      command=lambda: press("*"), height=HEIGHT, width=WIDTH) 
    multiply.grid(row=5, column=3) 
  
    divide = Button(gui, text=' / ', fg='black', bg='red', font=BTNFONT,
                    command=lambda: press("/"), height=HEIGHT, width=WIDTH) 
    divide.grid(row=6, column=3) 
  
    equal = Button(gui, text=' = ', fg='black', bg='red', font=BTNFONT,
                   command=equalpress, height=HEIGHT, width=WIDTH) 
    equal.grid(row=6, column=2) 
  
    clear = Button(gui, text='Clear', fg='black', bg='red', font=BTNFONT,
                   command=clear, height=HEIGHT, width=WIDTH) 
    clear.grid(row=6, column='1')

    # New button Square Root
    square = Button(gui, text='x^2', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('^2'), height=HEIGHT, width=WIDTH) 
    square.grid(row=2, column='0')

    # New button x^y
    raisetopower = Button(gui, text='x^y', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('x^y'), height=HEIGHT, width=WIDTH) 
    raisetopower.grid(row=2, column='1')

    # New button dec2hex
    dec2hex = Button(gui, text='dec2hex', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('dec2hex'), height=HEIGHT, width=WIDTH) 
    dec2hex.grid(row=2, column='2')

    # New button hex2dec
    hex2dec = Button(gui, text='hex2dec', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('hex2dec'), height=HEIGHT, width=WIDTH) 
    hex2dec.grid(row=2, column='3')

    # New button 'A'
    buttonA = Button(gui, text='A', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('A'), height=HEIGHT, width=WIDTH) 
    buttonA.grid(row=7, column='0')

    # New button 'B'
    buttonB = Button(gui, text='B', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('B'), height=HEIGHT, width=WIDTH) 
    buttonB.grid(row=7, column='1')

    # New button 'C'
    buttonC = Button(gui, text='C', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('C'), height=HEIGHT, width=WIDTH) 
    buttonC.grid(row=7, column='2')

    # New button 'D'
    buttonD = Button(gui, text='D', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('D'), height=HEIGHT, width=WIDTH) 
    buttonD.grid(row=8, column='0')

    # New button 'E'
    buttonE = Button(gui, text='E', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('E'), height=HEIGHT, width=WIDTH) 
    buttonE.grid(row=8, column='1')

    # New button 'F'
    buttonF = Button(gui, text='F', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('F'), height=HEIGHT, width=WIDTH) 
    buttonF.grid(row=8, column='2')

    # New button '.'
    buttonPoint = Button(gui, text='.', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('.'), height=HEIGHT, width=WIDTH) 
    buttonPoint.grid(row=7, column='3')

    # New button 'Lsh'
    buttonLsh = Button(gui, text='Lsh', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('Lsh'), height=HEIGHT, width=WIDTH) 
    buttonLsh.grid(row=2, column='4')

    # New button 'Rsh'
    buttonRsh = Button(gui, text='Rsh', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('Rsh'), height=HEIGHT, width=WIDTH) 
    buttonRsh.grid(row=3, column='4')

    # New button 'Or'
    buttonOr = Button(gui, text='Or', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('Or'), height=HEIGHT, width=WIDTH) 
    buttonOr.grid(row=4, column='4')

    # New button 'Xor'
    buttonXor = Button(gui, text='Xor', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('Xor'), height=HEIGHT, width=WIDTH) 
    buttonXor.grid(row=5, column='4')

    # New button 'Not'
    buttonNot = Button(gui, text='Not', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('Not'), height=HEIGHT, width=WIDTH) 
    buttonNot.grid(row=6, column='4')

    # New button 'And'
    buttonAnd = Button(gui, text='And', fg='black', bg='red', font=BTNFONT,
                   command=lambda: press('And'), height=HEIGHT, width=WIDTH) 
    buttonAnd.grid(row=7, column='4')

    # New button 'MS1'
    buttonMS1 = Button(gui, text='MS1', fg='black', bg='red', font=BTNFONT,
                   command=lambda: memoryset('mem1'), height=HEIGHT, width=WIDTH) 
    buttonMS1.grid(row=8, column='3')

    # New button 'MR1'
    buttonMR1 = Button(gui, text='MR1', fg='black', bg='red', font=BTNFONT,
                   command=lambda:memoryrecall('mem1'), height=HEIGHT, width=WIDTH) 
    buttonMR1.grid(row=8, column='4')

    # New button 'MS2'
    buttonMS2 = Button(gui, text='MS2', fg='black', bg='red', font=BTNFONT,
                   command=lambda: memoryset('mem2'), height=HEIGHT, width=WIDTH) 
    buttonMS2.grid(row=9, column='3')

    # New button 'MR2'
    buttonMR2 = Button(gui, text='MR2', fg='black', bg='red', font=BTNFONT,
                   command=lambda:memoryrecall('mem2'), height=HEIGHT, width=WIDTH) 
    buttonMR2.grid(row=9, column='4')

    # New label 'MS and MR '
    labelMSMR = Label(gui, text='MS sets only', fg='black', bg='white', font = "Arial 8 bold",
                      height=HEIGHT, width=12) 
    labelMSMR.grid(row=9, column='1')
    labelMSMR = Label(gui, text='after "=" set ', fg='black', bg='white', font = "Arial 8 bold",
                      height=HEIGHT, width=12) 
    labelMSMR.grid(row=9, column='2')
 
    
    # start the GUI 
    gui.mainloop() 
