from tkinter import *
from PIL import Image, ImageFilter
from PIL import ImageEnhance
import PIL
from PIL import Image
import os


def generateNewImage():
    print (w1.get(), w2.get(),w3.get(),w4.get(),E1.get(),E2.get())
    # Enlarged Image base size
    imageSize = int(E2.get())#3000
    # Contrast factor 0.0 gives a solid grey image,1.0 gives the original image.
    contrastFactor = w1.get()#1 #2.0 #1.2
    # An enhancement factor of 0.0 gives a blurred image, a factor of 1.0 gives the original image,
    # and a factor of 2.0 gives a sharpened image.
    sharpnessFactor = w2.get()#2.5 #2.1 #2.0
    #Brightness factor 1 will give initial brightness
    brightnessFactor = w3.get()#1#1.1
    #Color factor 1 will give initial color, 0 will give black&white, 100 will blow out the colors and 0.5 will be half way into a black&white image.
    colorFactor = w4.get()#2.4 #10#2.4
    
    # Initial image
    initialImage = E1.get()#'PIB2' #face'#'farmhouseside'#'PIB2'#'house' #'farmhouseside'
    
    # File format
    jpgfileFormat = '.jpg'
    pngfileFormat = '.png'
    
    # get a list of files from the current directory
    for root, dirs, files in os.walk(os.getcwd()):
        print("files->", files)
    # filter to the file that was specified in gui
    r = [s for s in files if s.startswith(initialImage)]
    #print("STARTSWITH->",r[0] if r else 'nomatch')
    
    # determine the image format type
    if r[0].endswith(jpgfileFormat):
        fileFormat = jpgfileFormat
    elif r[0].endswith(pngfileFormat):
        fileFormat = pngfileFormat
    
    # Contrasted image
    contrastImage  = initialImage + '_Contrast' + fileFormat
    # Image resized
    imageResized   = initialImage + '_Resized'  + fileFormat
    # Image sharpened
    imageSharpened = initialImage + '_Sharpened' + fileFormat
    # Image Brighter 
    imageBrighter  = initialImage + '_Brighter' + fileFormat
    # Image Color 
    imageColor  = initialImage + '_Color' + fileFormat
    
    #Read image
    im = Image.open( initialImage + fileFormat)
 
    #Display image
    im.show(title=initialImage)
    
    #Applying a filter to the image
    print("applying filtering")
    im_filtering = im.filter(ImageFilter#.CONTOUR)
                                        #.DETAIL)
                                        #.EDGE_ENHANCE)
                                        #.EDGE_ENHANCE_MORE)
                                        #.EMBOSS)
                                        #.FIND_EDGES)
                                        #.SMOOTH)
                                        #.SMOOTH_MORE)
                                        .SHARPEN)
                                        #.MedianFilter(3))
                                        #.Kernel((3, 3),(-1, -1, -1, -1, 9, -1, -1, -1, -1), 1, 0))
    
    #Applying a contrast to the image
    print("applying contrast")
    im_contrast = ImageEnhance.Contrast(im_filtering)
    im_contrast.enhance(contrastFactor).save(contrastImage)
    #im_contrast.enhance(contrastFactor).show("more contrast")
    
    #Applying more sharping on contrasted image 
    print("applying sharping")
    im_sharp = Image.open(contrastImage)
    im_sharp = ImageEnhance.Sharpness(im_sharp)
    ##im_sharp.enhance(sharpnessFactor).show("more sharpened")
    im_sharp.enhance(sharpnessFactor).save(imageSharpened)
    
    # Applying brightness to the image
    print("applying brightness")
    im_bright = Image.open(imageSharpened)
    im_bright = ImageEnhance.Brightness(im_bright)
    im_bright.enhance(brightnessFactor).save(imageBrighter)
    
    # Applying color to the image
    print("applying color")
    im_color = Image.open(imageBrighter)
    im_color = ImageEnhance.Color(im_color)
    im_color.enhance(colorFactor).save(imageColor)
    
    #Applying a sizing and saving the image
    print("applying resizing")
    basewidth = imageSize
    img = Image.open(imageColor)#imageBrighter) #imageSharpened)
    wpercent = (basewidth / float(img.size[0]))
    hsize = int((float(img.size[1]) * float(wpercent)))
    img = img.resize((basewidth, hsize), PIL.Image.ANTIALIAS)
    
    #Show and save image 
    img.show()
    img.save(imageResized)
    print("COMPLETE")
    
    
    
def reset():  
   print("RESET VALUES") 
   w1.set(1)
   w2.set(1)
   w3.set(1)
   w4.set(1)
   E1.delete(0,'end')
   E1.insert(0,'PIB2')
   E1.pack()
   E2.delete(0,'end')
   E2.insert(0,'3000')
   E2.pack()
   
# GUI SECTION 
master = Tk()

L1 = Label(master, text="INPUT IMAGE")
L1.pack()#side=LEFT)
E1 = Entry(master, bd=5)
E1.insert(0,'PIB2')
E1.pack()#side=RIGHT)

#input box file size
L2 = Label(master, text="OUTPUT SIZE")
L2.pack()#side=LEFT)
E2 = Entry(master, bd=5)
E2.insert(0,'3000')
E2.pack()#side=RIGHT)
 


w1 = Scale(master, from_=0, to=10.0, resolution=0.10, tickinterval=8,orient=HORIZONTAL)#.grid(row=0, column=0)
w1.set(1)
lcontrast = Label(master, text = "SET CONTRAST")
lcontrast.pack()
w1.pack()


w2 = Scale(master, from_=0, to=10.0,resolution=0.10,tickinterval=10, orient=HORIZONTAL)#.grid(row=0, column=1)#, orient=HORIZONTAL)
w2.set(1)
lsharpness = Label(master, text = "SET SHARPNESS")
lsharpness.pack()
w2.pack()

w3 = Scale(master, from_=0, to=10.0,resolution=0.10,tickinterval=10, orient=HORIZONTAL)#.grid(row=0, column=1)#, orient=HORIZONTAL)
w3.set(1)
lsharpness = Label(master, text = "SET BRIGHTNESS")
lsharpness.pack()
w3.pack()

w4 = Scale(master, from_=0, to=10.0,resolution=0.10,tickinterval=10, orient=HORIZONTAL)#.grid(row=0, column=1)#, orient=HORIZONTAL)
w4.set(1)
lcolor = Label(master, text = "SET COLOR")
lcolor.pack()
w4.pack()

Button(master, text='Show', command=generateNewImage).pack()

# reset defaults
Button(master, text='Reset', command=reset).pack(padx=5)


mainloop()
